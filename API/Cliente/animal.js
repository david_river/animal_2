let an = {
		id:0
}

function setId(id){
an.id =id
		}
$(document).ready(inicio);

function inicio() {

	cargarDatos();

	$("#btnGuardar").click(guardar);
	
	$("#btnEliminar").click(function (){
		eliminar(an.id)
	});

   $("#btnActualizar").click(modificar);
   $("#btnCancelar").click(reset);
}

function reset(){
	$("#nombre").val(null);
	$("#descripcion").val(null);
	
	$("#nombre2").val(null);
	$("#descripcion2").val(null);
}

function cargarDatos() {
	$.ajax({
		url: "http://localhost:8080/animales/",
		method: "Get",
		data:null,
		success: function (response){
			$("#datos").html("");
			
		for (let i = 0 ; i < response.length; i++) {
				$("#datos").append(
				"<tr>" +
					"<td>"+ response[i].clave + "</td>"+
					"<td>"+ response[i].nombre + "</td>"+
					"<td>"+ response[i].descripcion +" </td>"+
					"<td>"+ response[i].tipo + "</td>"+
					"<td>"+ response[i].fechaCreacion + "</td>"+
					"<td>"+
					"<button onclick='cargarRegistro("+ response[i].clave +
					")'type='button' class='btn btn-warning ml-3 mt-1' data-toggle='modal' data-target='#editar'><i class='fas fa-edit'></i>Editar</button>" +
                    "<button onclick='setId(" + response[i].clave +
                    ")' type='button' class='btn btn-danger ml-3 mt-1' style='color: black' data-toggle='modal' data-target='#eliminar'><i class='fas fa-trash-alt'></i> Eliminar</button>" +
                    "</td>" +
                    "</tr>"
                )
            }
        },
        error: function (response) {
            alert("Error: " + response);
        }
    });
}

function guardar(response){
	var fecha = new Date();
	$.ajax({
        url:"http://localhost:8080/animales/save",
        method:"Put",
        data:{
        	clave: null,
        	nombre:$("#nombre").val(),
            descripcion:$("#descripcion").val(),
            tipo:$("#tipo").val(),
            fecha: fecha
            
        },
        success:function(){
            cargarDatos();
            reset();
        },
        error: function (response) {
            alert("Error en la peticion: " + response)
        }
    })
}

function eliminar(id){
	$.ajax({
		url: "http://localhost:8080/animales/" +id,
		method: "Delete",
		success: function (){
			cargarDatos();
		},
		error: function (response) {
            alert("Error en la peticion: " + response)
        }
	})
}

function cargarRegistro(id){
	$.ajax({
		url:  "http://localhost:8080/animales/" +id,
		method: "Post",
		success: function (response){
			$("#clave2").val(response.clave)
            $("#nombre2").val(response.nombre)
            $("#descripcion2").val(response.descripcion)
            $("#tipo2").val(response.tipo)	
		},
		error: function (response) {
			alert("Error en la peticion: " + response)
		}	
	})
}

function modificar() {
    var id = $("#clave2").val();
    var fecha = new Date();
    $.ajax({
        url: "http://localhost:8080/animales/update/",
        method: "Put",
        data: {
            clave: id,
            nombre: $("#nombre2").val(),
            descripcion: $("#descripcion2").val(),
            tipo: $("#tipo2").val(),
            fecha: fecha
        },
        success: function (response) {
            cargarDatos();
            reset();
        },
        error: function (response) {
            alert("Error en la peticion: " + response);
            console.log(id);
        }
    });
}