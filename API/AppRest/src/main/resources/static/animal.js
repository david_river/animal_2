let animal = {
		id:0
}

function setIdAnimal(id){
an.id =id
		}
$(document).ready(inicio);

function inicio() {

	cargarDatos();

	$("#guardarAnimal").click(guardar);

	$("#eliminarAnimal").click(function (){
		eliminar(an.id)
	});

   $("#modificarAnimal").click(modificar);
   $("#btnCancelar").click(reset);
}

function reset(){
	$("#nombre").val(null);
	$("#descripcion").val(null);
	
	$("#nombre2").val(null);
	$("#descripcion2").val(null);
}

function cargarDatos(response) {
	$.ajax({
		url: "http://localhost:8080/animales/",
		method: "Get",
		data:null,
		success: function (response){
			$("#datos").html("");
			
		for (let i = 0 ; i < response.length; i++) {
				$("#datos").append(""
				"<tr>"+
					+"<td><strong>"+ response[i].clave+"<strong></td>"
					+"<td><strong>"+ response[i].nombre+"<strong></td>"
					+"<td><strong>"+ response[i].decripcion+"<strong></td>"
					+"<td><strong>"+ response[i].tipo+"<strong></td>"
					+"<td><strong>"+ response[i].fecha+"<strong></td>"
					+"<td>"+
					"<button onclick='cargarRegistro("+ response[i].clave +")'type='button' class='btn btn-warning ml-3 mt-1' data-toggle='modal' data-target='#editar'><i class='fas fa-edit'></i> <strong>Editar</strong></button>" +
                    "<button onclick='setId(" + response[i].clave +
                    ")' type='button' class='btn btn-danger ml-3 mt-1' style='color: black' data-toggle='modal' data-target='#eliminar'><i class='fas fa-trash-alt'></i> <strong>Eliminar</strong></button>" +
                    "</td>" +
                    "</tr>"
                )
            }
        },
        error: function (response) {
            alert("Error: " + response);
        }
    });
}

function guardar(response){
	var fecha new Date();
	$.ajax({
        url:"http://localhost:8080/animales/save",
        method:"Put",
        data:{
        	clave: null,
        	nombre:$("#nombre").val(),
            descripcion:$("#descripcion").val(),
            tipo:$("#tipo").val(),
            fecha: fecha
            
        },
        success:function(){
            cargarDatos();
            reset();
        },
        error: function (response) {
            alert("Error en la peticion: " + response)
        }
    })
}

function preEliminar(id) {
	$("#id").val(id);
	$("#modalEliminarAnimal").modal();
  }

  function eliminar() {
    $.ajax({
        url:"/animales/delete/"+id,
        method:"Post",
        data:null,
        success:function(response){
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}

function preModificar(id) {
    $("#modalModificarAnimal").modal();
    // alert(id);
    $.ajax({
        url:"/animales/getAnimal/"+id,
        method:"Get",
        success:function(response){
            $("#id").val(response.id);
            nombre:$("#nombre2").val()
            descripcion:$("#descripcion2").val()
            tipo:$("#tipo2").val()
            fecha: $("#fecha2").val(),
        },
        error:errorPeticion

    });
}

function modificar() {
    var id=$("#id").val();
    $.ajax({
        url:"/animales/update/"+id,
        method:"Get",
        data:{
            id:id,
            nombre:$("#nombre2").val()
            descripcion:$("#descripcion2").val()
            tipo:$("#tipo2").val()
            fecha: $("#fecha2").val(),
        },
        success:function(response){
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}
