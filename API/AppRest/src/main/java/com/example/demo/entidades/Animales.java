package com.example.demo.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;


@Entity

@Table(name = "animales")
public class Animales {
	
	@Id

	@Column(nullable = false, columnDefinition = "VARCHAR(50)")
	private Long clave;
	
	@NotNull
	@Column(nullable = false, columnDefinition = "VARCHAR(50)")
	private String nombre;

	@NotNull
	@Column(nullable = false, columnDefinition = "VARCHAR(200)")
	private String descripcion;

	@NotNull
	@Column(nullable = false, columnDefinition = "VARCHAR(50)")
	private String tipo;
	
	@Column(columnDefinition = "DATE DEFAULT CURRENT_TIMESTAMP")
	@JsonFormat(pattern = "yyyy-MM-dd HH-mm")
	private Date fechaCreacion;

	public Animales() {

	}

	public Animales(Long clave, String nombre, String descripcion, String tipo, Date fechaCreacion) {
		this.clave = clave;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.tipo = tipo;
		this.fechaCreacion = fechaCreacion;
	}

	
	public Animales(String nombre, String descripcion, String tipo, Date fechaCreacion) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.tipo = tipo;
		this.fechaCreacion = fechaCreacion;
	}

	public Long getClave() {
		return clave;
	}

	public void setClave(Long clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}
