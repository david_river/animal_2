
package com.example.demo.controlador;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.entidades.Animales;
import com.example.demo.repositorio.IAnimalRepository;

@Controller
@CrossOrigin
@RequestMapping("animales")
public class AnimalControlador {
	
	@Autowired
	IAnimalRepository ranimal;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public List<Animales> getAll() {
		return (List<Animales>) ranimal.findAll();
	}


	@PutMapping(value = "save", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public HashMap<String, String> save(@RequestParam Long clave, @RequestParam String nombre,
			@RequestParam String descripcion, @RequestParam String tipo, @RequestParam Date fecha) {
		
		Animales an = new Animales();
		
		clave = ranimal.count() + 5;
		
		an = new Animales(clave, nombre, descripcion, tipo, fecha);

		HashMap<String, String> jsonReturn = new HashMap<>();

		try {
			
			ranimal.save(an);

			
			jsonReturn.put("Estado", "OK");
			jsonReturn.put("Mensaje", "Registro guardado");

		
			return jsonReturn;
		} catch (Exception e) {

		
			jsonReturn.put("Estado", "Error");
			jsonReturn.put("Mensaje", "Registro no guardado");

			
			return jsonReturn;
		}
	}


	@PutMapping(value = "update", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public HashMap<String, String> update(@RequestParam Long clave, @RequestParam String nombre,
			@RequestParam String descripcion, @RequestParam String tipo, @RequestParam Date fecha) {
		
		Animales an = new Animales();
		
		an = new Animales(clave, nombre, descripcion, tipo, fecha);

		HashMap<String, String> jsonReturn = new HashMap<>();

		try {
		
			ranimal.save(an);

			
			jsonReturn.put("Estado", "OK");
			jsonReturn.put("Mensaje", "Registro guardado");

			
			return jsonReturn;
		} catch (Exception e) {

		
			jsonReturn.put("Estado", "Error");
			jsonReturn.put("Mensaje", "Registro no guardado");

		
			return jsonReturn;

		}
	}

	@PostMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Animales getMethodName(@PathVariable Long id) {
		return ranimal.findById(id).get();
	}

	@DeleteMapping(value ="{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Boolean delete(@PathVariable Long id) {
		Animales an= ranimal.findById(id).get();
		ranimal.delete(an);
		return true;
	}
}
